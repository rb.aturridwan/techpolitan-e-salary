<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Employees;
use Carbon\Carbon;
use App\Salary;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;

class ReportController extends Controller
{

    public function __construct()
    {
        $this->middleware("permission:read report")->only(["index", "salary"]);
    }

    public function salaryPrint(Request $r)
    {

        $employeeId = $r->get('employee_id') ?? null;

        $employeeData = Employees::find($employeeId);


        $bulan = $r->get('bulan') ?? null;
        $tahun = $r->get('tahun') ?? Carbon::now()->format('Y');

        $salaries = Salary::with('employee', 'employee.department', 'employee.position');
        if ($employeeId) {
            $salaries = $salaries->where('employee_id', $employeeId);
        }

        if ($bulan) {
            $salaries = $salaries->whereMonth('month', $bulan);
        }

        if ($tahun) {
            $salaries = $salaries->whereYear('month', $tahun);
        }

        $salaries = $salaries->get();

        // get data salaries 

        $data = [];

        foreach ($salaries as $salary) {
            $employee = $salary->employee;
            $date = Carbon::parse($salary->month);

            $sick = $employee->attendances()->where([
                [DB::raw("month(time)"), "=", $date->month],
                [DB::raw("year(time)"), "=", $date->year],
                ["description", "=", "sakit"]
            ])->orWhere("description", "izin")->count();
            $attend = $employee->totalAttend($date->month, $date->year);
            $absent = 22 - $attend;
            $late = $employee->totalLate($date->month, $date->year);
            $totalBenefits = array_reduce($employee->group->benefit()->pluck("amount")->toArray(), function ($carry, $item) {
                $carry += $item;
                return $carry;
            });
            $totalDeduction = $employee->salary - ((round($attend / 22)) * $employee->salary) + (($late * 15000));

            $totalBonus = 0;

            if ($salary->bonuses->count() > 0) {
                $bonuses = array_map(
                    function ($g, $p) use ($totalBonus) {
                        $c3 = isset($p[0]) ? $p[0] : "";
                        $c4 = isset($p[1]) ? $p[1] : "";
                        $totalBonus += $g['amount'];
                        return [$g["name"], $g["amount"], $c3, $c4];
                    },
                    $salary->bonuses->toArray(),
                    [["Potongan Absen", $employee->salary - ((round($attend / 22)) * $employee->salary)]]
                );
            } else {
                $bonuses = [["", "", "Potongan Absen", $employee->salary - ((round($attend / 22)) * $employee->salary)]];
            }



            $dataTables = [
                ["Gaji", $employee->salary, "Potongan Terlambat", $late * 15000]
            ];
            $dataTables = array_merge($dataTables, $bonuses);
            $total = (round($attend / 22)) * $employee->salary + $totalBenefits - (($late * 15000)) + (array_sum(array_column($bonuses, 1)));

            $data[] = [
                "employee" => $employee,
                "date" => $date,
                "sick" => $sick,
                "attend" => $attend,
                "absent" => $absent,
                "late" => $late,
                "totalBonus" => $totalBonus,
                "totalBenefits" => $totalBenefits + (array_sum(array_column($bonuses, 1))),
                "total" => $total,
                "totalDeduction" => $totalDeduction,
                "dataTables" => $dataTables,
            ];
        }

        // return $data;
        if(!$bulan){
            $periode = Carbon::parse("01-01-".$tahun);
        }

        if(!$tahun){
            $tahun = date("Y");
        }

        if($bulan && $tahun){
            $periode = Carbon::parse("01-".$bulan."-".$tahun);
        }



        $view = view("pdf.report-gaji", [
            "data" => $data,
            "month" => $bulan,
            "year" => $tahun,
            "periode" => $periode,
            "employee" => $employeeData,

        ]);

        // return $view;

        $pdf = App::make('dompdf.wrapper');
        $pdf->loadHTML($view->render());
        return $pdf->stream();

    }

    public function salary(Request $r){
        return view("admin.report.salary", [
            "title" => "Laporan Gaji | Techpolitan",
        ]);
    }

    public function autocomplete(Request $request)
    {
        $data = Employees::select("name as value", "id")
            ->where('name', 'LIKE', '%' . $request->get('search') . '%')
            ->get();

        return response()->json($data);
    }
}
