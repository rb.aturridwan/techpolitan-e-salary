-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Aug 02, 2022 at 10:26 AM
-- Server version: 10.4.21-MariaDB
-- PHP Version: 7.4.29

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `laravel`
--

-- --------------------------------------------------------

--
-- Table structure for table `attendances`
--

CREATE TABLE `attendances` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `employee_id` bigint(20) UNSIGNED NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time` datetime NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `attendances`
--

INSERT INTO `attendances` (`id`, `employee_id`, `description`, `time`, `created_at`, `updated_at`) VALUES
(1, 1, '', '2021-05-01 08:29:00', '2021-06-12 21:16:09', '2021-06-12 21:16:09'),
(2, 1, '', '2021-05-01 17:31:00', '2021-06-12 21:16:09', '2021-06-12 21:16:09'),
(3, 1, '', '2021-05-02 08:29:00', '2021-06-12 21:16:09', '2021-06-12 21:16:09'),
(4, 1, '', '2021-05-02 17:33:00', '2021-06-12 21:16:09', '2021-06-12 21:16:09'),
(5, 1, '', '2021-05-03 08:24:00', '2021-06-12 21:16:09', '2021-06-12 21:16:09'),
(6, 1, '', '2021-05-03 17:37:00', '2021-06-12 21:16:09', '2021-06-12 21:16:09'),
(7, 1, '', '2021-05-04 08:26:00', '2021-06-12 21:16:10', '2021-06-12 21:16:10'),
(8, 1, '', '2021-05-04 17:24:00', '2021-06-12 21:16:10', '2021-06-12 21:16:10'),
(9, 1, '', '2021-05-05 08:40:00', '2021-06-12 21:16:10', '2021-06-12 21:16:10'),
(10, 1, '', '2021-05-05 17:38:00', '2021-06-12 21:16:10', '2021-06-12 21:16:10'),
(11, 1, '', '2021-05-06 08:36:00', '2021-06-12 21:16:10', '2021-06-12 21:16:10'),
(12, 1, '', '2021-05-06 17:21:00', '2021-06-12 21:16:10', '2021-06-12 21:16:10'),
(13, 1, '', '2021-05-07 08:20:00', '2021-06-12 21:16:10', '2021-06-12 21:16:10'),
(14, 1, '', '2021-05-07 17:21:00', '2021-06-12 21:16:10', '2021-06-12 21:16:10'),
(15, 1, '', '2021-05-08 08:29:00', '2021-06-12 21:16:10', '2021-06-12 21:16:10'),
(16, 1, '', '2021-05-08 17:35:00', '2021-06-12 21:16:10', '2021-06-12 21:16:10'),
(17, 1, '', '2021-05-09 08:31:00', '2021-06-12 21:16:10', '2021-06-12 21:16:10'),
(18, 1, '', '2021-05-09 17:35:00', '2021-06-12 21:16:10', '2021-06-12 21:16:10'),
(19, 1, '', '2021-05-10 08:21:00', '2021-06-12 21:16:10', '2021-06-12 21:16:10'),
(20, 1, '', '2021-05-10 17:30:00', '2021-06-12 21:16:10', '2021-06-12 21:16:10'),
(21, 1, '', '2021-05-11 08:27:00', '2021-06-12 21:16:10', '2021-06-12 21:16:10'),
(22, 1, '', '2021-05-11 17:24:00', '2021-06-12 21:16:10', '2021-06-12 21:16:10'),
(23, 2, '', '2021-05-12 08:37:00', '2021-06-12 21:16:10', '2021-06-12 21:16:10'),
(24, 2, '', '2021-05-12 17:30:00', '2021-06-12 21:16:10', '2021-06-12 21:16:10'),
(25, 2, '', '2021-05-13 08:40:00', '2021-06-12 21:16:10', '2021-06-12 21:16:10'),
(26, 1, '', '2021-05-13 17:37:00', '2021-06-12 21:16:10', '2021-06-12 21:16:10'),
(27, 1, '', '2021-05-14 08:34:00', '2021-06-12 21:16:10', '2021-06-12 21:16:10'),
(28, 1, '', '2021-05-14 17:24:00', '2021-06-12 21:16:10', '2021-06-12 21:16:10'),
(29, 1, '', '2021-05-15 08:23:00', '2021-06-12 21:16:10', '2021-06-12 21:16:10'),
(30, 1, '', '2021-05-15 17:40:00', '2021-06-12 21:16:10', '2021-06-12 21:16:10'),
(31, 1, '', '2021-05-16 08:36:00', '2021-06-12 21:16:10', '2021-06-12 21:16:10'),
(32, 1, '', '2021-05-16 17:40:00', '2021-06-12 21:16:11', '2021-06-12 21:16:11'),
(33, 1, '', '2021-05-17 08:30:00', '2021-06-12 21:16:11', '2021-06-12 21:16:11'),
(34, 1, '', '2021-05-17 17:25:00', '2021-06-12 21:16:11', '2021-06-12 21:16:11'),
(35, 1, '', '2021-05-18 08:27:00', '2021-06-12 21:16:11', '2021-06-12 21:16:11'),
(36, 1, '', '2021-05-18 17:20:00', '2021-06-12 21:16:11', '2021-06-12 21:16:11'),
(37, 1, '', '2021-05-19 08:25:00', '2021-06-12 21:16:11', '2021-06-12 21:16:11'),
(38, 1, '', '2021-05-19 17:28:00', '2021-06-12 21:16:11', '2021-06-12 21:16:11'),
(39, 1, '', '2021-05-20 08:39:00', '2021-06-12 21:16:11', '2021-06-12 21:16:11'),
(40, 1, '', '2021-05-20 17:24:00', '2021-06-12 21:16:11', '2021-06-12 21:16:11'),
(41, 1, '', '2021-05-21 08:26:00', '2021-06-12 21:16:11', '2021-06-12 21:16:11'),
(42, 1, '', '2021-05-21 17:20:00', '2021-06-12 21:16:11', '2021-06-12 21:16:11'),
(43, 1, '', '2021-05-22 08:40:00', '2021-06-12 21:16:11', '2021-06-12 21:16:11'),
(44, 1, '', '2021-05-22 17:27:00', '2021-06-12 21:16:11', '2021-06-12 21:16:11'),
(46, 3, '', '2021-06-13 08:20:10', '2021-06-13 14:49:12', '2021-06-13 14:49:12'),
(47, 3, '', '2021-06-13 17:30:00', '2021-06-13 14:49:12', '2021-06-13 14:49:12'),
(48, 3, '', '2021-06-11 08:00:00', '2021-06-14 17:00:00', NULL),
(49, 3, '', '2021-06-11 17:30:00', '2021-06-14 17:00:00', NULL),
(50, 2, NULL, '2022-06-23 14:15:29', '2022-06-23 07:15:38', '2022-06-23 07:15:38'),
(51, 2, NULL, '2022-06-23 14:15:29', '2022-06-23 07:16:04', '2022-06-23 07:16:04'),
(52, 1, NULL, '2022-06-23 14:16:16', '2022-06-23 07:16:46', '2022-06-23 07:16:46');

-- --------------------------------------------------------

--
-- Table structure for table `benefits`
--

CREATE TABLE `benefits` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `group_id` bigint(20) UNSIGNED NOT NULL,
  `amount` bigint(20) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `benefits`
--

INSERT INTO `benefits` (`id`, `name`, `group_id`, `amount`, `created_at`, `updated_at`) VALUES
(1, 'BPJS KS', 1, 400000, NULL, NULL),
(2, 'Transport', 1, 50000, NULL, NULL),
(3, 'Transport', 2, 25000, NULL, NULL),
(4, 'Makan', 3, 25000, NULL, NULL),
(5, 'transport', 3, 25000, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `bonuses`
--

CREATE TABLE `bonuses` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `salary_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `amount` bigint(20) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `bonuses`
--

INSERT INTO `bonuses` (`id`, `salary_id`, `name`, `amount`, `created_at`, `updated_at`) VALUES
(1, 6, 'Makan', 100000, '2022-07-26 21:47:20', '2022-07-26 21:47:20'),
(2, 8, 'Makan', 24998, '2022-07-26 22:05:48', '2022-07-26 22:05:48'),
(3, 8, 'Lembur', 100000, '2022-07-26 22:05:48', '2022-07-26 22:05:48'),
(4, 9, 'Makan', 25000, '2022-07-26 22:41:26', '2022-07-26 22:41:26');

-- --------------------------------------------------------

--
-- Table structure for table `complains`
--

CREATE TABLE `complains` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `employee_id` bigint(20) UNSIGNED NOT NULL,
  `complain` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `complains`
--

INSERT INTO `complains` (`id`, `employee_id`, `complain`, `created_at`, `updated_at`) VALUES
(1, 1, 'Mohon izin saya terlambat', '2022-07-16 01:27:53', '2022-07-16 01:27:53'),
(2, 1, 'izin tidak masuk', '2022-07-23 02:01:04', '2022-07-23 02:01:04');

-- --------------------------------------------------------

--
-- Table structure for table `departments`
--

CREATE TABLE `departments` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `departments`
--

INSERT INTO `departments` (`id`, `name`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Support', '1', NULL, NULL),
(2, 'Wb Development', 'Aktif', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `employees`
--

CREATE TABLE `employees` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `position_id` bigint(20) UNSIGNED NOT NULL,
  `group_id` bigint(20) UNSIGNED NOT NULL,
  `department_id` int(11) NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `gender` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `salary` bigint(20) NOT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `norek` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `employees`
--

INSERT INTO `employees` (`id`, `user_id`, `position_id`, `group_id`, `department_id`, `name`, `gender`, `address`, `phone`, `email`, `salary`, `status`, `created_at`, `updated_at`, `norek`) VALUES
(1, 2, 1, 2, 1, 'Karina Mawardah', 'Perempuan', 'Kp.Dadap', '081291897637', 'karina@tech.com', 2800000, 'Aktif', NULL, NULL, '9876512345'),
(2, 3, 1, 1, 1, 'Dede Kurniawan', 'Laki-Laki', 'Cisauk', '081234569543', 'dede@tech.com', 2000000, 'Aktif', NULL, NULL, '9876134567'),
(3, 6, 1, 1, 1, 'Citra Tannia', 'Perempuan', 'Jelupang', '081317426683', 'Citra@tech.com', 5500000, 'Aktif', NULL, NULL, '8263923927');

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `groups`
--

CREATE TABLE `groups` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `groups`
--

INSERT INTO `groups` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'A1', NULL, NULL),
(2, 'A2', NULL, NULL),
(3, 'A3', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2020_05_09_125145_create_positions_table', 1),
(5, '2020_05_09_125513_create_departments_table', 1),
(6, '2020_05_09_131454_create_employees_table', 1),
(7, '2020_05_09_131612_create_attendances_table', 1),
(8, '2020_05_09_132241_create_benefits_table', 1),
(9, '2020_05_09_134924_create_permission_tables', 1),
(10, '2021_05_05_034303_add_role_user', 1),
(11, '2021_05_28_143626_create_groups_table', 1),
(12, '2021_06_11_182218_add_column_norek_employees_table', 1),
(13, '2021_06_12_055111_create_salaries', 1),
(14, '2021_06_12_055118_create_complains', 1),
(15, '2021_06_12_055231_create_bonuses', 1);

-- --------------------------------------------------------

--
-- Table structure for table `model_has_permissions`
--

CREATE TABLE `model_has_permissions` (
  `permission_id` bigint(20) UNSIGNED NOT NULL,
  `model_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `model_has_roles`
--

CREATE TABLE `model_has_roles` (
  `role_id` bigint(20) UNSIGNED NOT NULL,
  `model_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `model_has_roles`
--

INSERT INTO `model_has_roles` (`role_id`, `model_type`, `model_id`) VALUES
(1, 'App\\User', 1),
(1, 'App\\User', 7),
(2, 'App\\User', 2),
(2, 'App\\User', 3),
(2, 'App\\User', 6);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `guard_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `positions`
--

CREATE TABLE `positions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `positions`
--

INSERT INTO `positions` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'IT Support', NULL, NULL),
(2, 'Website', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `guard_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `guard_name`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'web', '2021-06-22 20:09:26', '2021-06-22 20:09:26'),
(2, 'user', 'web', '2021-06-22 20:09:26', '2021-06-22 20:09:26');

-- --------------------------------------------------------

--
-- Table structure for table `role_has_permissions`
--

CREATE TABLE `role_has_permissions` (
  `permission_id` bigint(20) UNSIGNED NOT NULL,
  `role_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `salaries`
--

CREATE TABLE `salaries` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `employee_id` bigint(20) UNSIGNED NOT NULL,
  `month` date NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `salaries`
--

INSERT INTO `salaries` (`id`, `employee_id`, `month`, `created_at`, `updated_at`) VALUES
(6, 1, '2022-07-01', '2022-07-26 21:47:20', '2022-07-26 21:47:20'),
(7, 2, '2022-07-12', '2022-07-26 21:50:03', '2022-07-26 21:50:03'),
(8, 3, '2022-07-06', '2022-07-26 22:05:48', '2022-07-26 22:05:48'),
(9, 1, '2022-07-12', '2022-07-26 22:41:26', '2022-07-26 22:41:26'),
(10, 1, '2021-05-31', '2022-07-29 01:30:04', '2022-07-29 01:30:04');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `role_id` bigint(20) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `role_id`, `created_at`, `updated_at`) VALUES
(1, 'Admin Tech', 'admin@mail.com', '2021-06-22 20:09:26', '$2y$10$9GwvNbGngo2v0q3JDon0n.bkQC9KtlBavOmzQVUsaTRyaQfjoX1tW', NULL, 1, '2021-06-22 20:09:26', '2021-06-22 20:09:26'),
(2, 'Karina Mawardah', 'karina@tech.com', NULL, '$2y$10$9GwvNbGngo2v0q3JDon0n.bkQC9KtlBavOmzQVUsaTRyaQfjoX1tW', NULL, 2, '2021-06-22 20:12:47', '2021-06-22 20:14:43'),
(3, 'Dede Kurniawan', 'dede@tech.com', NULL, '$2y$10$9GwvNbGngo2v0q3JDon0n.bkQC9KtlBavOmzQVUsaTRyaQfjoX1tW', NULL, 2, '2021-06-22 20:13:37', '2021-06-22 20:14:10'),
(6, 'Citra Tannia', 'Citra@tech.com', NULL, '$2y$10$MhxKMmVU.GRu0y1zol1wzODE.S6hfenEz.4FddTgz5hYhUP9l1RYa', NULL, NULL, '2022-07-23 01:57:55', '2022-07-23 01:57:55'),
(7, 'Admin2', 'admin2@tech.com', NULL, '$2y$10$YyPO9rjNru0rRADgx0hXiunh8gyVnRTCw9WRrE.LOl..ZBfk2FLsi', NULL, NULL, '2022-07-23 02:00:12', '2022-07-23 02:00:12');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `attendances`
--
ALTER TABLE `attendances`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `benefits`
--
ALTER TABLE `benefits`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bonuses`
--
ALTER TABLE `bonuses`
  ADD PRIMARY KEY (`id`),
  ADD KEY `bonuses_salary_id_foreign` (`salary_id`);

--
-- Indexes for table `complains`
--
ALTER TABLE `complains`
  ADD PRIMARY KEY (`id`),
  ADD KEY `complains_employee_id_foreign` (`employee_id`);

--
-- Indexes for table `departments`
--
ALTER TABLE `departments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `employees`
--
ALTER TABLE `employees`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `groups`
--
ALTER TABLE `groups`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `model_has_permissions`
--
ALTER TABLE `model_has_permissions`
  ADD PRIMARY KEY (`permission_id`,`model_id`,`model_type`),
  ADD KEY `model_has_permissions_model_id_model_type_index` (`model_id`,`model_type`);

--
-- Indexes for table `model_has_roles`
--
ALTER TABLE `model_has_roles`
  ADD PRIMARY KEY (`role_id`,`model_id`,`model_type`),
  ADD KEY `model_has_roles_model_id_model_type_index` (`model_id`,`model_type`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `positions`
--
ALTER TABLE `positions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `role_has_permissions`
--
ALTER TABLE `role_has_permissions`
  ADD PRIMARY KEY (`permission_id`,`role_id`),
  ADD KEY `role_has_permissions_role_id_foreign` (`role_id`);

--
-- Indexes for table `salaries`
--
ALTER TABLE `salaries`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `attendances`
--
ALTER TABLE `attendances`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=53;

--
-- AUTO_INCREMENT for table `benefits`
--
ALTER TABLE `benefits`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `bonuses`
--
ALTER TABLE `bonuses`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `complains`
--
ALTER TABLE `complains`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `departments`
--
ALTER TABLE `departments`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `employees`
--
ALTER TABLE `employees`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `groups`
--
ALTER TABLE `groups`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `positions`
--
ALTER TABLE `positions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `salaries`
--
ALTER TABLE `salaries`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
