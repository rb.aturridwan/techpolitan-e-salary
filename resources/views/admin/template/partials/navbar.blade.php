<header class="main-header">
    <!-- Logo -->
    <a href="index2.html" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>T</b>ECH</span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>TECHPOLITAN</b></span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>


      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
            <li class="dropdown notifications-menu">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                <i class="fa fa-bell-o"></i>
                <span id="notification-count" class="label label-warning"></span>
                </a>
                <ul class="dropdown-menu">
                    <li class="header" id="notification-header">You have 0 notifications</li>
                    <li>
                        <ul class="menu" id="notification-list">
                            
                        </ul>
                    </li>
                    <li class="footer"><a href="#">View all</a></li>
                </ul>
            </li>
        </ul>
    </div>
      

    </nav>
  </header>