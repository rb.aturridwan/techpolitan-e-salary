<!-- jQuery 3 -->
<script src="{{ asset('admin/bower_components/jquery/dist/jquery.min.js') }}"></script>
<!-- jQuery UI 1.11.4 -->
<script src="{{ asset('admin/bower_components/jquery-ui/jquery-ui.min.j') }}s"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Bootstrap 3.3.7 -->
<script src="{{ asset('admin/bower_components/bootstrap/dist/js/bootstrap.min.js') }}"></script>
<!-- Morris.js charts -->
<script src="{{ asset('admin/bower_components/raphael/raphael.min.js') }}"></script>
<script src="{{ asset('admin/bower_components/morris.js/morris.min.js') }}"></script>
<!-- Sparkline -->
<script src="{{ asset('admin/bower_components/jquery-sparkline/dist/jquery.sparkline.min.js') }}"></script>
<!-- jvectormap -->
<script src="{{ asset('admin/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js') }}"></script>
<script src="{{ asset('admin/plugins/jvectormap/jquery-jvectormap-world-mill-en.js') }}"></script>
<!-- jQuery Knob Chart -->
<script src="{{ asset('admin/bower_components/jquery-knob/dist/jquery.knob.min.js') }}"></script>
<!-- daterangepicker -->
<script src="{{ asset('admin/bower_components/moment/min/moment.min.js') }}"></script>
<script src="{{ asset('admin/bower_components/bootstrap-daterangepicker/daterangepicker.js') }}"></script>

<!-- DataTables -->
<script src="{{ asset('admin/bower_components/datatables.net/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('admin/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>

<!-- datepicker -->
<script src="{{ asset('admin/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="{{ asset('admin/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js') }}"></script>
<!-- Slimscroll -->
<script src="{{ asset('admin/bower_components/jquery-slimscroll/jquery.slimscroll.min.js') }}"></script>
<!-- FastClick -->
<script src="{{ asset('admin/bower_components/fastclick/lib/fastclick.js') }}"></script>
<!-- AdminLTE App -->
<script src="{{ asset('admin/dist/js/adminlte.min.js') }}"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="{{ asset('admin/dist/js/pages/dashboard.js') }}"></script>
<!-- AdminLTE for demo purposes -->
<script src="{{ asset('admin/dist/js/demo.js') }}"></script>

<script>

    $(document).ready(function() {
      // check notification every 1 minute
      generateNotification();
    })

    // function check notif every 1 minute
    function checkNotif(){
      $.ajax({
        url: "{{ route('admin.notification.check') }}",
        type: 'GET',
        // json
        dataType: 'json',
        success: function(res) {

          if(res.status){
            // show notification
            renderNotification(res.data);
            setTimeout(function(){
              checkNotif();
            }, 60000);
          }else{
            setTimeout(function(){
              checkNotif();
            }, 60000);
          }
        }
      });
    }

    function renderNotification(data){
      var html = '';
      let unreadNotif = data.filter(function(item){
        return item.read_at == null;
      });
      console.log(unreadNotif.length);
      $('#notification-count').html(unreadNotif.length > 0 ? unreadNotif.length  : '');
      $('#notification-header').text(`You have ${unreadNotif.length} unread notification`);
      $.each(data, function(index, val) {
        html += `<li>
                  <a data-toggle="tooltip" data-placement="top" title="${val.data}"  href="${val.link}" target="_blank">
                    <i class="fa fa-users text-aqua"></i> ${val.data}
                  </a>
                </li>`;
      });
      $('#notification-list').html(html);
    }

    // function create notification
    function generateNotification(){
      $.ajax({
        url: "{{ route('admin.notification.generate') }}",
        type: 'POST',
        data: {
        "_token": "{{ csrf_token() }}",
        },
        // json
        dataType: 'json',
        success: function(res) {
          if(res.status){
            // show notification
            checkNotif();
          }
        }
      });
    }
</script>

@stack('script')