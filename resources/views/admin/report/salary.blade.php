@extends('admin.template.default')

@section('content')

<section class="content-header">
  <h1>
    Laporan Penggajian
  </h1>
  <ol class="breadcrumb">
  <li><a href="{{ route('admin.dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>
    <li class="active">Laporan Penggajian</li>
  </ol>
</section>

<section class="content">
    <div class="row">
      <div class="col-xs-12">
        <div class="box">
          <div class="box-header">
            <h3 class="box-title">Laporan Penggajian</h3>
          </div>
          <!-- /.box-header -->
          <form action="{{route('admin.report.salary.print')}}" target="_blank">
          <div class="box-body">
             <div class="container">
                <div class="row">
                  <div class="col-md-3">
                    <div class="form-group">
                      <label for="">Karyawan</label>
                      <input type="text" class="form-control" id="employee" placeholder="Cari Karyawan">
                      <input type="hidden" class="form-control" id="employee_id" name="employee_id" placeholder="Karyawan">
                      <span>*kosongkan untuk semua karyawan</span>
                    </div>
                  </div>

                  <div class="col-md-2">
                    <div class="form-group">
                      <label for="">Bulan</label>
                      <select name="bulan" id="bulan" class="form-control">
                        <option value="">Pilih</option>
                        <option value="1">Januari</option>
                        <option value="2">Februari</option>
                        <option value="3">Maret</option>
                        <option value="4">April</option>
                        <option value="5">Mei</option>
                        <option value="6">Juni</option>
                        <option value="7">Juli</option>
                        <option value="8">Agustus</option>
                        <option value="9">September</option>
                        <option value="10">Oktober</option>
                        <option value="11">November</option>
                        <option value="12">Desember</option>
                      </select>
                      <span>*kosongkan untuk semua bulan</span>
                    </div>
                  </div>

                <div class="col-md-2">
                  <div class="form-group">
                    <label for="">Tahun</label>
                    <select name="tahun" id="tahun" class="form-control">
                      <option value="">Pilih</option>
                      @for($i = date('Y'); $i >= date('Y')-5; $i--)
                        <option value="{{ $i }}">{{ $i }}</option>
                      @endfor
                    </select>
                    <span>*kosongkan untuk tahun ini</span>
                  </div>
                </div>

                <div class="col-md-3">
                  <div class="form-group">
                    <label style="display:block" for="">&nbsp;</label>
                    <button class="btn btn-primary" id="btn-cari">Cetak Laporan</button>
                  </div>
                </div>

             </div>
          </div>
          </form>
        </div>
      </div>
    </div>
</section>

<form action="" method="post" id="deleteForm">
  @csrf
  @method("DELETE")
  <input type="submit" value="Hapus" style="display: none">
</form>

@endsection

@push('script')

  <script src="{{ asset('admin/plugins/bs-notify.min.js') }}"></script>

  @include('admin.template.partials.alert')

    <script>
        $(function() {
          $('#dataTable').DataTable({
            processing: true,
            serverSide: true,
            ajax: '{{ route('admin.complain.data') }}',
            columns: [
              { data: 'DT_RowIndex', orderable: false, searchable: false },
              @if(auth()->user()->hasRole('admin'))

              { data: 'employee' },
              @endif
              { data: 'complain' },
              { data: 'action' },
            ]
          })

          $( "#employee" ).autocomplete({
            // allow clear
            
            source: function( request, response ) {
              $.ajax({
                url: "{{ route('admin.report.autocomplete') }}",
                type: 'GET',
                dataType: "json",
                data: {
                  search: request.term
                },
                success: function( data ) {
                  response( data );
                }
              });
            },
            select: function (event, ui) {
              $('#employee').val(ui.item.label);
              $('#employee_id').val(ui.item.id);

              return false;
            }
          });

          $( "#employee" ).on('change', function() {
             if($(this).val() == '') {
                $('#employee_id').val('');
             }
          });


        })
    </script>

@endpush
