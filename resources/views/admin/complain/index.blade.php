@extends('admin.template.default')

@section('content')

<section class="content-header">
  <h1>
    Komplain
  </h1>
  <ol class="breadcrumb">
  <li><a href="{{ route('admin.dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>
    <li class="active">Komplain</li>
  </ol>
</section>

<section class="content">
    <div class="row">
      <div class="col-xs-12">
        <div class="box">
          <div class="box-header">
            <h3 class="box-title">Komplain</h3>
            <div style="float: right!important">
              @if(!auth()->user()->hasRole('admin'))
                <a href="{{ route('admin.complain.create') }}" class="btn btn-primary btn-sm">+ Ajukan Komplain</a>
              @endif
            </div>
          </div>
          <!-- /.box-header -->
          <div class="box-body">
            <table id="dataTable" class="table table-bordered table-hover">
              <thead>
              <tr>
                <th>NO</th>
                @if(auth()->user()->hasRole('admin'))
                <th>Nama Karyawan</th>
                @endif
                <th>Pesan</th>
                <th>Action</th>
              </tr>
              </thead>

              <tbody>

              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
</section>

<form action="" method="post" id="deleteForm">
  @csrf
  @method("DELETE")
  <input type="submit" value="Hapus" style="display: none">
</form>

@endsection

@push('script')

  <script src="{{ asset('admin/plugins/bs-notify.min.js') }}"></script>

  @include('admin.template.partials.alert')

    <script>
        $(function() {
          $('#dataTable').DataTable({
            processing: true,
            serverSide: true,
            ajax: '{{ route('admin.complain.data') }}',
            columns: [
              { data: 'DT_RowIndex', orderable: false, searchable: false },
              @if(auth()->user()->hasRole('admin'))

              { data: 'employee' },
              @endif
              { data: 'complain' },
              { data: 'action' },
            ]
          })
        })
    </script>

@endpush
