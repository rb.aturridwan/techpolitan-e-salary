<a href="{{ route('admin.employee.edit', $model) }}" class="btn btn-warning" data-toggle="tooltip" data-placement="top" title="Ubah"><i class="fa fa-pencil"></i></a>

<button href="{{ route('admin.employee.destroy', $model) }}" class="btn btn-danger" id="delete" data-toggle="tooltip" data-placement="top" title="Hapus"><i class="fa fa-trash"></i></button>

<!-- check if working end_date is less 30 day from today -->
@if($model->type == 'kontrak' && $model->workingTimes()->exists() && $model->workingTimes()->orderBy('id','desc')->first()->end_date < \Carbon\Carbon::now()->addDays(30))

<a href="{{ route('admin.employee.addcontract', $model->id) }}" class="btn btn-success" id="add_kontrak" data-toggle="tooltip" data-placement="top" title="Perpanjang Kontrak"><i class="fa fa-plus"></i></a>
@endif

<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
<script>
    $('button#delete').on('click', function(e){
        e.preventDefault();

        var href = $(this).attr('href');

        Swal.fire({
        title: 'Apakah Yakin Hapus Data?',
        text: "Data yang sudah dihapus tidak bisa dikembalikan !",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Hapus'
        }).then((result) => {
        if (result.value) {
            
            document.getElementById('deleteForm').action = href;
            document.getElementById('deleteForm').submit();

            Swal.fire(
            'Terhapus',
            'Data kamu berhasil dihapus',
            'success'
            )
        }
    })

    })
</script>