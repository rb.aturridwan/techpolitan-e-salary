
<table style="width: 100%; height: auto">
        <tr>
            <th colspan="3" align="left"><h2>TECHPOLITAN</h2></th>
            <th></th>
            <th></th>
            <th></th>
            <th></th>
            <th>LAPORAN PENGGAJIAN</th>
        </tr>
        @if($employee != null && $employee != "")
        <tr>
            <th colspan="2" align="left">Nama</th>
            <th align="center">:</th>
            <th align="left">{{$employee->name}}</th>
            <th></th>
            <th></th>
            <th></th>
        </tr>
        <tr>
            <th colspan="2" align="left">Jabatan</th>
            <th align="center">:</th>
            <th align="left">{{$employee->position->name}}</th>
            <th></th>
            <th></th>
            <th></th>
        </tr>
        @endif
        <tr>
            <th colspan="2" align="left">Periode</th>
            <th align="center">:</th>
            <th align="left">@if($month != "" ){{ $periode->isoFormat("MMMM YYYY")}} @else {{$periode->isoFormat("YYYY")}} @endif</th>
            <th></th>
            <th></th>
            <th></th>
        </tr>
</table>
<table style="margin-top: 40px; width: 100%" border="1" cellspacing=".5px" cellpadding="5px">
    <thead>
        <tr>
            @if($employee == null || $employee == "")
            <th align="center">Nama</th>
            <th align="center">Jabatan</th>
            @endif
            @if($month == "" )
            <th align="center">Periode</th>
            @endif
            <th align="center">Gaji Pokok</th>
            <th align="center">Tunjangan</th>
            <th align="center">Potongan</th>
            <th align="center">Take Home Pay</th>
        </tr>
    </thead>
    <tbody style="margin-top: 20px;">
        @foreach($data as $item)
        <tr>
            @if($employee == null || $employee == "")
            <td>{{$item['employee']->name}}</td>
            <td>{{$item['employee']->position->name}}</td>
            @endif
            @if($month == "" )
            <td>{{$item['date']->isoFormat("MMMM YYYY")}}</td>
            @endif
            <td align="right">{{number_format($item['employee']->salary, 0, ',', '.')}}</td>
            <td align="right">{{number_format($item['totalBenefits'], 0, ',', '.')}}</td>
            <td align="right">{{number_format($item['totalDeduction'], 0, ',', '.')}}</td>
            <td align="right">{{number_format($item['total'], 0, ',', '.')}}</td>
        </tr>
        @endforeach
    </tbody>
</table>

<div>
    <h4>
        <span style="float: left">* @if($month == "") {{$periode->firstOfYear()->isoFormat("DD MMMM")." - ".$periode->lastOfYear()->isoFormat("DD MMMM YYYY")}} @else {{$periode->firstOfMonth()->isoFormat("DD MMMM")." - ".$periode->lastOfMonth()->isoFormat("DD MMMM YYYY")}} @endif </span>
        <span style="float: right">BSD, {{Carbon\Carbon::now()->isoFormat("DD MMMM YYYY")}}</span>
    </h4>
</div>

<div style="clear: both; megin-top: 100px">
</div>
